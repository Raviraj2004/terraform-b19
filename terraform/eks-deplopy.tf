provider "aws" {
    region "eu-west-2"
}

data "aws_iam_policy_document" "my-cluster-role" {
    statement {
      effect = "Allow"

      principals {
        type        = "Service"
        identifiers = ["eks.amazon.com"]
      }
      actions  = ["sts:AssumeRole"]   
      
    }
}

resource "aws_iam_role" "new-role" {
    name                = "new-cluster1"
    assume_role_policy  = data.aws_iam_policy_document.clusterassume-role.json

}

resource "aws_iam_role_policy_attachment" "eks-policy" {
    policy_arn  = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
    role        = aws_iam_role.new-role.name
}

resources "aws_iam_role_policy_attachment" "controller-policy" {
    policy_arn   = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
    role         = aws_iam_role.new-role.name
}

resource "aws_eks_cluster" "my-eksCluster" {
    name        = "ravi-cluster"
    role_arn    = aws_iam_role.new-role.arn
 

    vpc_config {
        subnet_ids = [
            "vpc-062170bc700a42963"
            "vpc-062170bc700a42963"
        ]
        }
    depends_on = [
        aws_iam_role_policy_attachment.
    ]    
    }
}