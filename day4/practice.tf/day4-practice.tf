provider "aws" {
    region = "eu-west-2"
}

terraform {
   backend "s3" {
     region   = "eu-west-2"
     bucket   = "just-bucket-1"
     key      = "./terraform.tfstate"

   } 
}

data "aws_security_group" "existing_sg" {
    name = "launch-wizard-2"
    vpc_id = "vpc-01e17f519eb327568"

}

resource "aws_security_group_rule" "allow_ssh" {
    type              = "ingress"
    to_port           = 22
    protocol          = "TCP"
    from_port         = 22
    cidr_blocks       =  ["0.0.0.0/0"]
    security_group_id = data.aws_security_group.existing_sg.id

}

resource "aws_security_group_rule" "allow_http"  {
    type              = "ingress"
    to_port           = 80
    protocol          = "TCP"
    from_port         = 80
    cidr_blocks       = ["0.0.0.0/0"]
    security_group_id = data.aws_security_group.existing_sg.id

}
 resource "aws_instance" "new_instance" {
    ami                    = "ami-0171207a7acd2a570"
    instance_type          = "t3.micro"
    key_name               = "london-key-new" 
    vpc_security_group_ids = [data.aws_security_group.existing_sg.id]



    connection {
        type        = "ssh"
        user        = "ec2-user"
        private_key = file("./private-key.pem")
        host        = self.public_ip
}

    provisioner "remote-exec"  {
    inline = [
       "sudo yum install httpd -y",
       "sudo systemctl start httpd",
       "sudo systemctl enable httpd"
       ]
}

    provisioner "local-exec"  {
         command   = "echo '<h1> hello world' > index.html"
         # command = "echo ${self.public_ip} >> ips.txt"
}

    provisioner "file" {
        source      = "index.html"
        destination = "/var/www/html/index.html"
    }
         
 }
